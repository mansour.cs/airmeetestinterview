plugins {
    id("com.android.library")
    kotlin("android")
//    kotlin("android.extensions")
    kotlin("kapt")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    buildToolsVersion(Versions.BUILD_TOOLS)

    defaultConfig {
        minSdkVersion(Versions.MIN_SDK)
        targetSdkVersion(Versions.TARGET_SDK)
        versionCode = Versions.VERSION_CODE
        versionName = Versions.VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            isDebuggable = false
            isZipAlignEnabled = true
            isJniDebuggable = false
            isRenderscriptDebuggable = false
//            signingConfig = signingConfigs.getByName("release")
        }
    }

    flavorDimensions("airmee")
    productFlavors{
        create("directAPK") {
            dimension = "airmee"
            buildConfigField( "String", "RELEASE_MARKET", "\"directAPK\"")
        }
        create("playStore") {
            dimension = "airmee"
            buildConfigField( "String", "RELEASE_MARKET", "\"playStore\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}


dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.KOTLIN}")
    implementation("androidx.annotation:annotation:${Versions.ANNOTATION}")
    implementation("com.squareup.retrofit2:retrofit:${Versions.RETROFIT}")
    implementation("com.squareup.retrofit2:converter-gson:${Versions.RETROFIT}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE}")
    implementation("com.squareup.okhttp3:okhttp:${Versions.OKHTTP3}")
    implementation("com.squareup.okhttp3:logging-interceptor:${Versions.OKHTTP3}")
    implementation("org.koin:koin-android:${Versions.KOIN}")
    implementation("org.koin:koin-core-ext:${Versions.KOIN}")
    testImplementation("org.koin:koin-test:${Versions.KOIN}")
    implementation("androidx.room:room-runtime:${Versions.ROOM}")
    kapt("androidx.room:room-compiler:${Versions.ROOM}")
}