package com.airmee.businesscore

import android.app.Application
import com.airmee.businesscore.config.CoreConfig

open class BusinessCoreApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        CoreConfig.initializeCore(this)
    }
}