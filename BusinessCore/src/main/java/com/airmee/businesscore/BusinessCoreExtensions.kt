package com.airmee.businesscore

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


fun LocalDate.formatted(): String {
    return this.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
}