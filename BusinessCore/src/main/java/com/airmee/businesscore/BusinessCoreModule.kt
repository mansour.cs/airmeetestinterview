package com.airmee.businesscore

import com.airmee.businesscore.features.book.repository.BookApartmentRepositoryImpl
import com.airmee.businesscore.features.book.repository.IBookApartmentRepository
import com.airmee.businesscore.features.book.service.BookApartmentServiceImpl
import com.airmee.businesscore.features.book.service.IBookApartmentService
import com.airmee.businesscore.features.search.data.api.ISearchApiCaller
import com.airmee.businesscore.features.search.data.api.SearchApiCallerImpl
import com.airmee.businesscore.features.search.repository.ApartmentSearchRepositoryImpl
import com.airmee.businesscore.features.search.repository.IApartmentSearchRepository
import com.airmee.businesscore.features.search.service.ApartmentSearchServiceImpl
import com.airmee.businesscore.features.search.service.IApartmentSearchService
import com.airmee.businesscore.features.user.service.IUserService
import com.airmee.businesscore.features.user.service.UserServiceImpl
import org.koin.dsl.module


val searchModule = module {
    single<ISearchApiCaller> { SearchApiCallerImpl() }
    single<IApartmentSearchRepository> { ApartmentSearchRepositoryImpl(get()) }
    single<IApartmentSearchService> { ApartmentSearchServiceImpl(get()) }
}
val userModule = module {
    single<IUserService> { UserServiceImpl() }
}
val bookModule = module {
    single<IBookApartmentRepository> { BookApartmentRepositoryImpl() }
    single<IBookApartmentService> { BookApartmentServiceImpl(get()) }
}

val coreModules = listOf(
    searchModule,
    userModule,
    bookModule
)