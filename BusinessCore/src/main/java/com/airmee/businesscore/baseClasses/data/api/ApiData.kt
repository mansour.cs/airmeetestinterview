package com.airmee.businesscore.baseClasses

import androidx.annotation.Keep
import com.airmee.businesscore.baseClasses.data.api.ApiDataStatus
import com.airmee.businesscore.baseClasses.data.api.NetworkError

@Keep
internal class ApiData<T>(
        val status: ApiDataStatus,
        var data: T? = null,
        var networkError: NetworkError? = null,
        var throwable: Throwable? = null
) {
    companion object DataWrapperAttrs {

        fun <T> success(data: T): ApiData<T> {
            return ApiData<T>(
                ApiDataStatus.SUCCESS,
                data
            )
        }

        fun <T> error(error: Throwable): ApiData<T> {
            return ApiData<T>(
                status = ApiDataStatus.ERROR,
                throwable = error
            )
        }

        fun <T> error(
            networkError: NetworkError,
            error: Throwable? = null
        ): ApiData<T> {
            return ApiData<T>(
                status = ApiDataStatus.ERROR,
                networkError = networkError,
                throwable = error
            )
        }

    }

}