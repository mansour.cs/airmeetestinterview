package com.airmee.businesscore.baseClasses.data.api

internal enum class ApiDataStatus {
    SUCCESS,
    ERROR
}