package com.airmee.businesscore.baseClasses.data.api

import okhttp3.Headers
import okhttp3.Response
import okhttp3.ResponseBody

internal class NetworkError(val code: Int,
                            val errorBody: ResponseBody?,
                            val headers: Headers,
                            val message: String,
                            val raw: Response)