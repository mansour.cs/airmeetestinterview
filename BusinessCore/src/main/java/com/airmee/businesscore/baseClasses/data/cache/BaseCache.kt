package com.airmee.businesscore.baseClasses.data.cache

import com.airmee.businesscore.config.CoreConfig

internal open class BaseCache {
    var receiveTime : Long = 0
    protected fun initCacheTime() {
        receiveTime = System.currentTimeMillis()
    }
    var isValid = System.currentTimeMillis() - receiveTime <= CoreConfig.cacheValidTime


}