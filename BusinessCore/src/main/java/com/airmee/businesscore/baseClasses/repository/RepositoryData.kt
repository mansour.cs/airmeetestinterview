package com.airmee.businesscore.baseClasses

import androidx.annotation.Keep
import com.airmee.businesscore.baseClasses.repository.RepositoryDataStatus
import com.airmee.businesscore.baseClasses.repository.RepositoryError

@Keep
internal class RepositoryData<T>(
        val status: RepositoryDataStatus,
        var data: T? = null,
        var repositoryError: RepositoryError? = null
) {
    companion object DataWrapperAttrs {

        fun <T> success(data: T): RepositoryData<T> {
            return RepositoryData<T>(
                RepositoryDataStatus.SUCCESS,
                data
            )
        }

        fun <T> error(repositoryError: RepositoryError?): RepositoryData<T> {
            return RepositoryData<T>(
                status = RepositoryDataStatus.ERROR,
                repositoryError = repositoryError
            )
        }

        fun <T> error(
            data: T,
            repositoryError: RepositoryError??
        ): RepositoryData<T> {
            return RepositoryData<T>(
                status = RepositoryDataStatus.ERROR,
                data = data,
                repositoryError = repositoryError
            )
        }

    }

}