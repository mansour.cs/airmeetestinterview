package com.airmee.businesscore.baseClasses.repository

internal enum class RepositoryDataStatus {
    SUCCESS,
    ERROR
}