package com.airmee.businesscore.baseClasses.repository

data class RepositoryError(val type: RepositoryErrorType,
                           val message: String,
                           val code: Int,
                           val throwable: Throwable? = null)