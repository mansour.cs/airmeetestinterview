package com.airmee.businesscore.baseClasses.repository

enum class RepositoryErrorType {
    Api,
    Persistent,
    Cache
}