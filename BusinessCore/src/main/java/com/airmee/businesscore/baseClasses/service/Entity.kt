package com.airmee.businesscore.baseClasses.service

/* Every data which can be sent out of the BusinessCore should be an implementation of Entity
* App level should be depended on Entities and not Dto classes */
interface Entity {
}