package com.airmee.businesscore.baseClasses

import androidx.annotation.Keep
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.baseClasses.service.ServiceMessage

@Keep
class ServiceData<T>(
        val status: ServiceDataStatus,
        var data: T? = null,
        var serviceMessage: ServiceMessage? = null
) {
    companion object DataWrapperAttrs {

        fun <T> success(data: T, serviceMessage: ServiceMessage? = null): ServiceData<T> {
            return ServiceData<T>(
                    ServiceDataStatus.SUCCESS,
                    data,
                    serviceMessage
            )
        }

        fun <T> error(data: T? = null, serviceMessage: ServiceMessage): ServiceData<T> {
            return ServiceData<T>(
                status = ServiceDataStatus.ERROR,
                data = data,
                serviceMessage = serviceMessage
            )
        }

    }



}




