package com.airmee.businesscore.baseClasses.service

enum class ServiceDataStatus {
    SUCCESS,
    ERROR
}