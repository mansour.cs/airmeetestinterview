package com.airmee.businesscore.baseClasses.service


/*This class is used for sending messages like errors, warnings and info to out of business core */
data class ServiceMessage(val type: ServiceMessageType, val message: String, val code: Int) : Entity {
    companion object {
        const val ERROR_CODE_UNKNOWN = 100
    }

}
