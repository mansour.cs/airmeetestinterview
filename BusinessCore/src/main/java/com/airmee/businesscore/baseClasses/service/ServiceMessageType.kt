package com.airmee.businesscore.baseClasses.service

enum class ServiceMessageType {
    Error, Warning, Info
}