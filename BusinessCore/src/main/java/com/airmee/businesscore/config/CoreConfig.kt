package com.airmee.businesscore.config

import android.app.Application

object CoreConfig {
    const val baseUrl = "https://s3-eu-west-1.amazonaws.com/product.versioning.com/"
    const val cacheValidTime : Long = 60000L;

    lateinit var applicationContext: Application

    fun initializeCore(applicationContext: Application){
        CoreConfig.applicationContext = applicationContext
    }
}