package com.airmee.businesscore.features.book.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "BookedApartments")
internal data class BookedApartmentDto(@PrimaryKey val id: String,
                                       val dateFrom: String,
                                       val dateTo: String) {

}
