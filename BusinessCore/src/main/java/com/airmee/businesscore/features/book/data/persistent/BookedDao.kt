package com.airmee.businesscore.features.book.data.persistent

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.airmee.businesscore.features.book.data.BookedApartmentDto

@Dao
internal interface BookedDao {
    @Query("Select * from BookedApartments")
    fun getBookedList(): List<BookedApartmentDto>

    @Insert
    fun bookNewApartment(dto: BookedApartmentDto)

    @Delete
    fun deleteBookedApartment(dto: BookedApartmentDto)
}