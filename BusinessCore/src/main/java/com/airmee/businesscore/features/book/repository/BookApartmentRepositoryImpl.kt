package com.airmee.businesscore.features.book.repository

import com.airmee.businesscore.baseClasses.RepositoryData
import com.airmee.businesscore.baseClasses.repository.RepositoryError
import com.airmee.businesscore.baseClasses.repository.RepositoryErrorType
import com.airmee.businesscore.features.book.data.BookedApartmentDto
import com.airmee.businesscore.tools.BusinessCoreDB
import kotlinx.coroutines.*
import java.lang.Exception
import java.lang.IllegalArgumentException

internal class BookApartmentRepositoryImpl: IBookApartmentRepository {

    override suspend fun getBookedApartmentList(): RepositoryData<List<BookedApartmentDto>> = coroutineScope {
        val bookList: Deferred<List<BookedApartmentDto>?> = async(Dispatchers.IO) {
            BusinessCoreDB.getAppDataBase()?.getBookedDao()?.getBookedList()
        }
        with(bookList.await()) {
            if (this != null)
                return@coroutineScope RepositoryData.success(this)
            else
                return@coroutineScope RepositoryData.error(RepositoryError(RepositoryErrorType.Persistent,"Error in fetching data", 0))
        }
    }

    override suspend fun bookNewApartment(bookedApartmentDto: BookedApartmentDto): RepositoryData<Boolean> = coroutineScope {
        val result: Deferred<RepositoryData<Boolean>> = async(Dispatchers.IO) {
            try {
                BusinessCoreDB.getAppDataBase()?.getBookedDao()?.
                bookNewApartment(bookedApartmentDto)
                return@async RepositoryData.success(true)
            } catch (e: Exception) {
                return@async RepositoryData.error(RepositoryError(RepositoryErrorType.Persistent,"Error in saving data", 0, e))
            }
        }
        return@coroutineScope result.await()
    }

    override suspend fun deleteBookedApartment(id: String): RepositoryData<Boolean> = coroutineScope {
        val result: Deferred<RepositoryData<Boolean>> = async(Dispatchers.IO) {
            try {
                BusinessCoreDB.getAppDataBase()?.getBookedDao()?.
                deleteBookedApartment(BookedApartmentDto(id, "", ""))
                return@async RepositoryData.success(true)
            } catch (e: Exception) {
                return@async RepositoryData.error(RepositoryError(RepositoryErrorType.Persistent,"Error in deleting data", 0, e))
            }
        }
        return@coroutineScope result.await()
    }
}