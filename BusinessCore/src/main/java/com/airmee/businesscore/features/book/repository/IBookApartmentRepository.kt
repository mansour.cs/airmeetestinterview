package com.airmee.businesscore.features.book.repository

import androidx.lifecycle.MutableLiveData
import com.airmee.businesscore.baseClasses.RepositoryData
import com.airmee.businesscore.features.book.data.BookedApartmentDto

internal interface IBookApartmentRepository {
    suspend fun getBookedApartmentList(): RepositoryData<List<BookedApartmentDto>>
    suspend fun bookNewApartment(bookedApartmentDto: BookedApartmentDto):  RepositoryData<Boolean>
    suspend fun deleteBookedApartment(id: String):  RepositoryData<Boolean>
}