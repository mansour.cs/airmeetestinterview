package com.airmee.businesscore.features.book.service

import com.airmee.businesscore.baseClasses.ServiceData
import com.airmee.businesscore.baseClasses.repository.RepositoryDataStatus
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.baseClasses.service.ServiceMessage
import com.airmee.businesscore.baseClasses.service.ServiceMessageType
import com.airmee.businesscore.features.book.data.BookedApartmentDto
import com.airmee.businesscore.features.book.repository.IBookApartmentRepository
import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import com.airmee.businesscore.features.book.service.entities.toEntity
import com.airmee.businesscore.features.search.service.IApartmentSearchService
import com.airmee.businesscore.formatted
import org.koin.java.KoinJavaComponent
import java.time.LocalDate

internal class BookApartmentServiceImpl(private val repository: IBookApartmentRepository): IBookApartmentService {

    private val searchService: IApartmentSearchService by KoinJavaComponent.inject(
        IApartmentSearchService::class.java
    )

    override suspend fun getBookedApartmentList(): ServiceData<List<BookedApartmentEntity>> {

        val repoData = repository.getBookedApartmentList()
        if (repoData.status == RepositoryDataStatus.SUCCESS) {
            val serviceData = searchService.getApartmentList()
            if (serviceData.status == ServiceDataStatus.SUCCESS) {
                var entityList = repoData.data!!.map{it.toEntity(serviceData.data!!)}.sortedBy { it.dateFrom }
                return ServiceData.success(entityList)
            }
        }
        //else repoData.status == RepositoryDataStatus.ERROR
        return ServiceData.error(serviceMessage = ServiceMessage(
            ServiceMessageType.Error,
            "Error in fetching data",
            ServiceMessage.ERROR_CODE_UNKNOWN)
        )




    }

    override suspend fun bookNewApartment(id: String,
                                          dateFrom: LocalDate,
                                          dateTo: LocalDate): ServiceData<Boolean> {

        val repoData = repository.bookNewApartment(
            BookedApartmentDto(id, dateFrom.formatted(), dateTo.formatted())
        )
        return when(repoData.status) {
            RepositoryDataStatus.SUCCESS -> ServiceData.success(repoData.data!!)
            RepositoryDataStatus.ERROR -> {
                ServiceData.error(serviceMessage = ServiceMessage(
                    ServiceMessageType.Error,
                    "Error in saving data",
                    ServiceMessage.ERROR_CODE_UNKNOWN)
                )
            }
        }
    }

    override suspend fun deleteBookedApartment(id: String): ServiceData<Boolean> {

        val repoData = repository.deleteBookedApartment(id)
        return when(repoData.status) {
            RepositoryDataStatus.SUCCESS -> ServiceData.success(repoData.data!!)
            RepositoryDataStatus.ERROR -> {
                ServiceData.error(serviceMessage = ServiceMessage(
                    ServiceMessageType.Error,
                    "Error in delete data",
                    ServiceMessage.ERROR_CODE_UNKNOWN)
                )
            }
        }
    }

    override suspend fun getBookedApartmentListIds(): ServiceData<List<String>> {

        val repoData = repository.getBookedApartmentList()
        return when(repoData.status) {
            RepositoryDataStatus.SUCCESS -> {
                var entityList = repoData.data!!.map{it.id }
                ServiceData.success(entityList)
            }
            RepositoryDataStatus.ERROR -> {
                ServiceData.error(serviceMessage = ServiceMessage(
                    ServiceMessageType.Error,
                    "Error in fetching data",
                    ServiceMessage.ERROR_CODE_UNKNOWN)
                )
            }
        }
    }
}