package com.airmee.businesscore.features.book.service

import com.airmee.businesscore.baseClasses.ServiceData
import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import java.time.LocalDate

interface IBookApartmentService {
    suspend fun getBookedApartmentList(): ServiceData<List<BookedApartmentEntity>>
    suspend fun bookNewApartment(id: String, dateFrom: LocalDate, dateTo: LocalDate) : ServiceData<Boolean>
    suspend fun deleteBookedApartment(id: String) : ServiceData<Boolean>
    suspend fun getBookedApartmentListIds(): ServiceData<List<String>>
}