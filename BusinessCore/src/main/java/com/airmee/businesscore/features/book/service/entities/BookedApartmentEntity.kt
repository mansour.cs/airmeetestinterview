package com.airmee.businesscore.features.book.service.entities

import com.airmee.businesscore.baseClasses.service.Entity
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import java.time.LocalDate

data class BookedApartmentEntity(val apartmentItemEntity: ApartmentItemEntity?,
                                 val dateFrom: LocalDate,
                                 val dateTo: LocalDate) : Entity
