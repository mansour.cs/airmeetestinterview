package com.airmee.businesscore.features.book.service.entities

import com.airmee.businesscore.features.book.data.BookedApartmentDto
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal fun BookedApartmentDto.toEntity(apartmentEntities: List<ApartmentItemEntity>) :BookedApartmentEntity {
    val lDateFrom = LocalDate.parse(dateFrom, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
    val lDateTo = LocalDate.parse(dateTo, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
    val entities = apartmentEntities.filter { it.id == id }
    val entity = if(entities.isNotEmpty()) entities[0] else null
    return BookedApartmentEntity(entity, lDateFrom, lDateTo)
}