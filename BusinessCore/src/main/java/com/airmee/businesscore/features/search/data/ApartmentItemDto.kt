package com.airmee.businesscore.features.search.data

import com.airmee.businesscore.baseClasses.data.Dto
import com.google.gson.annotations.SerializedName

internal data class ApartmentItemDto(@SerializedName("id") val id: String,
                            @SerializedName("bedrooms") val bedrooms: Int,
                            @SerializedName("name") val name: String,
                            @SerializedName("latitude") val latitude: Double,
                            @SerializedName("longitude") val longitude: Double
) : Dto
