package com.airmee.businesscore.features.search.data.api

import com.airmee.businesscore.baseClasses.ApiData
import com.airmee.businesscore.features.search.data.ApartmentItemDto

internal interface ISearchApiCaller {
    suspend fun call(): ApiData<List<ApartmentItemDto>>
}
