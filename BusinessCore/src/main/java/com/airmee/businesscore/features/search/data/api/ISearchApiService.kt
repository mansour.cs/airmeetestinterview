package com.airmee.businesscore.features.search.data.api

import com.airmee.businesscore.features.search.data.ApartmentItemDto
import retrofit2.Response
import retrofit2.http.GET

internal interface ISearchApiService {

    @GET("apartments.json")
    suspend fun requestApartmentList(): Response<List<ApartmentItemDto>>
}