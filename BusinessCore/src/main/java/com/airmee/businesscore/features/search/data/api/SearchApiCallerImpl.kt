package com.airmee.businesscore.features.search.data.api

import com.airmee.businesscore.baseClasses.ApiData
import com.airmee.businesscore.baseClasses.data.api.NetworkError
import com.airmee.businesscore.tools.RetrofitBuilder
import com.airmee.businesscore.features.search.data.ApartmentItemDto
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import retrofit2.Response

internal class SearchApiCallerImpl: ISearchApiCaller {
    private val service: ISearchApiService by lazy {
        RetrofitBuilder.defaultBuilder().create(ISearchApiService::class.java)
    }

    override suspend fun call(): ApiData<List<ApartmentItemDto>> = coroutineScope {

        val result: Deferred<Response<List<ApartmentItemDto>>> = async {
            service.requestApartmentList()
        }
        return@coroutineScope if (result.await().isSuccessful)
            ApiData.success(result.await().body()!!)
        else {
            val error = result.await()
            ApiData.error(networkError = NetworkError(
                error.code(),
                error.errorBody(),
                error.headers(),
                error.message(),
                error.raw())
            )
        }
    }
}
