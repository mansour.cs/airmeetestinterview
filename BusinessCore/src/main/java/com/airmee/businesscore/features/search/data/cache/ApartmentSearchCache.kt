package com.airmee.businesscore.features.search.data.cache

import com.airmee.businesscore.baseClasses.data.cache.BaseCache
import com.airmee.businesscore.features.search.data.ApartmentItemDto

internal object ApartmentSearchCache: BaseCache() {
    var apartments: List<ApartmentItemDto> = listOf()
        get() {return field}
        set(value) {
            field = value
            initCacheTime()
        }

}