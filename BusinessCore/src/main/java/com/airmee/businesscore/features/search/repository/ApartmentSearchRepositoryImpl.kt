package com.airmee.businesscore.features.search.repository

import com.airmee.businesscore.baseClasses.RepositoryData
import com.airmee.businesscore.baseClasses.data.api.ApiDataStatus
import com.airmee.businesscore.baseClasses.repository.RepositoryError
import com.airmee.businesscore.baseClasses.repository.RepositoryErrorType
import com.airmee.businesscore.features.search.data.api.ISearchApiCaller
import com.airmee.businesscore.features.search.data.ApartmentItemDto
import com.airmee.businesscore.features.search.data.cache.ApartmentSearchCache

internal class ApartmentSearchRepositoryImpl(private val searchApiCaller: ISearchApiCaller): IApartmentSearchRepository {

    override suspend fun getApartmentList(): RepositoryData<List<ApartmentItemDto>> {

        return if (ApartmentSearchCache.isValid) { //Use cache
            RepositoryData.success(data = ApartmentSearchCache.apartments)
        } else { // call api
            val apiData = searchApiCaller.call()
            if (apiData.status == ApiDataStatus.SUCCESS) {
                ApartmentSearchCache.apartments = apiData.data!!
                RepositoryData.success(data =  ApartmentSearchCache.apartments)
            } else {
                RepositoryData.error(RepositoryError(RepositoryErrorType.Api, "Api error", 0))
            }
        }
    }
}