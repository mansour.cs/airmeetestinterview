package com.airmee.businesscore.features.search.repository

import androidx.lifecycle.MutableLiveData
import com.airmee.businesscore.baseClasses.RepositoryData
import com.airmee.businesscore.features.search.data.ApartmentItemDto

internal interface IApartmentSearchRepository {
    suspend fun getApartmentList(): RepositoryData<List<ApartmentItemDto>>
}