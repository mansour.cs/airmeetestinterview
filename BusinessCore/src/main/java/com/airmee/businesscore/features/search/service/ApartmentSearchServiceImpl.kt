package com.airmee.businesscore.features.search.service

import com.airmee.businesscore.baseClasses.ServiceData
import com.airmee.businesscore.baseClasses.repository.RepositoryDataStatus
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.baseClasses.service.ServiceMessage
import com.airmee.businesscore.baseClasses.service.ServiceMessageType
import com.airmee.businesscore.features.book.service.IBookApartmentService
import com.airmee.businesscore.features.search.repository.IApartmentSearchRepository
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.features.user.service.entities.UserEntity
import org.koin.java.KoinJavaComponent
import java.time.LocalDate

internal class ApartmentSearchServiceImpl(private val repository: IApartmentSearchRepository)
    : IApartmentSearchService {

    private val bookService: IBookApartmentService by KoinJavaComponent.inject(IBookApartmentService::class.java)

    override suspend fun getApartmentList(userEntity: UserEntity?,
                                          bedrooms: Int?,
                                          dateFrom: LocalDate,
                                          dateTo: LocalDate): ServiceData<List<ApartmentItemEntity>> {

        val apartmentsData = getApartmentList(userEntity, bedrooms)
        return if (apartmentsData.status == ServiceDataStatus.SUCCESS) {
            var entityList =
                apartmentsData.data!!.filter { it.availableDateFrom <= dateFrom && it.availableDateTo >= dateTo }

            ServiceData.success(entityList)
        } else {
            apartmentsData
        }
    }

    override suspend fun getApartmentList(
        userEntity: UserEntity?,
        bedrooms: Int?
    ): ServiceData<List<ApartmentItemEntity>> {
        var apartmentsData = getApartmentList()
        return if (apartmentsData.status == ServiceDataStatus.SUCCESS) {
            val bookData = bookService.getBookedApartmentListIds()
            var entityList = sortAndApplyFilters(apartmentsData.data!!, userEntity, bedrooms, bookData.data)

            ServiceData.success(entityList)
        } else {
            apartmentsData
        }
    }

    override suspend fun getApartmentList(): ServiceData<List<ApartmentItemEntity>> {
        var repositoryData = repository.getApartmentList()

        if (repositoryData.status == RepositoryDataStatus.SUCCESS) {
            var entityList = repositoryData.data?.map { it.toEntity() }

            entityList?.let {
                return@getApartmentList ServiceData.success(it)
            }

            return ServiceData.error(serviceMessage = ServiceMessage(ServiceMessageType.Error,
                "Error in filtering data",
                ServiceMessage.ERROR_CODE_UNKNOWN)
            )
        } else {  //repositoryData.status == RepositoryDataStatus.ERROR
            return ServiceData.error(serviceMessage = ServiceMessage(ServiceMessageType.Error,
                "Error in fetching data",
                ServiceMessage.ERROR_CODE_UNKNOWN)
            )
        }
    }

    private fun sortAndApplyFilters(
        entityList: List<ApartmentItemEntity>,
        userEntity: UserEntity?,
        bedrooms: Int?,
        bookedList: List<String>?
    ): List<ApartmentItemEntity> {
        var filteredList = entityList
        //sort by closest to user
        userEntity?.let {
            var userLat = it.defLatitude
            var userLong = it.defLongitude
            it.latitude?.let { userLat = it }
            it.longitude?.let { userLong = it }
            filteredList = filteredList.sortedBy {
                it.distanceFrom(userLat to userLong)
            }
        }

        //filter by bedrooms
        bedrooms?.let {
            filteredList = filteredList.filter { it.bedrooms == bedrooms }
        }

        //filter by removing booked items
        bookedList?.let {
            filteredList = filteredList.filter { it.id !in bookedList }
        }


        return filteredList

    }

}