package com.airmee.businesscore.features.search.service

import com.airmee.businesscore.baseClasses.ServiceData
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.features.user.service.entities.UserEntity
import java.time.LocalDate

interface IApartmentSearchService {
    suspend fun getApartmentList(userEntity: UserEntity? = null, bedrooms: Int? = null, dateFrom: LocalDate, dateTo: LocalDate): ServiceData<List<ApartmentItemEntity>>
    suspend fun getApartmentList(userEntity: UserEntity? = null, bedrooms: Int? = null): ServiceData<List<ApartmentItemEntity>>
    suspend fun getApartmentList(): ServiceData<List<ApartmentItemEntity>>
}