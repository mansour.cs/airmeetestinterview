package com.airmee.businesscore.features.search.service.entities

import com.airmee.businesscore.baseClasses.service.Entity
import java.time.LocalDate
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

data class ApartmentItemEntity(val id: String,
                               val bedrooms: Int,
                               val name: String,
                               val latitude: Double,
                               val longitude: Double,
                               val availableDateFrom: LocalDate,
                               val availableDateTo: LocalDate
) : Entity {
    fun distanceFrom(coordinate: Pair<Double, Double>): Double {
        return sqrt((latitude - coordinate.first).pow(2) + (longitude - coordinate.second).pow(2))
    }
}
