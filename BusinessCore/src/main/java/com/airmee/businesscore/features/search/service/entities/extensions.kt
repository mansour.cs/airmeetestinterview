package com.airmee.businesscore.features.search.service

import com.airmee.businesscore.features.search.data.ApartmentItemDto
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import java.time.LocalDate

internal fun ApartmentItemDto.toEntity(): ApartmentItemEntity {
    var dateFrom = LocalDate.now()
    var dateTo = dateFrom.plusDays((this.bedrooms*3).toLong())  //Only for generate different dates for different items and not randomly!

    return ApartmentItemEntity(id, bedrooms, name, latitude, longitude, dateFrom, dateTo)
}