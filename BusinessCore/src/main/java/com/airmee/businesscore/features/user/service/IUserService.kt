package com.airmee.businesscore.features.user.service

import com.airmee.businesscore.features.user.service.entities.UserEntity

interface IUserService {
    fun getDefaultUser(): UserEntity
    fun getDefaultUser(latitude: Double, longitude: Double): UserEntity
}