package com.airmee.businesscore.features.user.service

import com.airmee.businesscore.features.user.service.entities.UserEntity

internal class UserServiceImpl: IUserService {
    override fun getDefaultUser(): UserEntity {
        return UserEntity("Mansour", "Khaleghi", 59.329440, 18.069124)
    }

    override fun getDefaultUser(latitude: Double, longitude: Double): UserEntity {
        return UserEntity("Mansour",
            "Khaleghi",
            59.329440,
            18.069124,
            latitude,
            longitude)
    }
}