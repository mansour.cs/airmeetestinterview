package com.airmee.businesscore.features.user.service.entities

import com.airmee.businesscore.baseClasses.service.Entity
import java.net.ProtocolFamily

data class UserEntity(val name: String,
                      val family: String,
                      val defLatitude: Double,
                      val defLongitude: Double,
                      val latitude: Double? = null,
                      val longitude: Double? = null): Entity