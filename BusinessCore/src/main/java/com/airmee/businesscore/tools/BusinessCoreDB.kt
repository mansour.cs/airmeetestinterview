package com.airmee.businesscore.tools

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.airmee.businesscore.config.CoreConfig
import com.airmee.businesscore.features.book.data.BookedApartmentDto
import com.airmee.businesscore.features.book.data.persistent.BookedDao
import org.koin.experimental.property.inject


@Database(entities = [BookedApartmentDto::class], version = 1)
internal abstract class BusinessCoreDB : RoomDatabase() {
    abstract fun getBookedDao(): BookedDao

    companion object {
        private var INSTANCE: BusinessCoreDB? = null

        fun getAppDataBase(): BusinessCoreDB? {
            if (INSTANCE == null){

                synchronized(BusinessCoreDB::class){
                    INSTANCE = Room.databaseBuilder(CoreConfig.applicationContext, BusinessCoreDB::class.java, "BusinessCoreDB").build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}