package com.airmee.businesscore.tools

import com.airmee.businesscore.config.CoreConfig
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    fun defaultBuilder(baseUrl: String? = null): Retrofit {
        val gson = GsonBuilder()
            .enableComplexMapKeySerialization()
            .setLenient()
            .create()

        val retrofit = Retrofit.Builder()
            .baseUrl(CoreConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))

        baseUrl?.let { retrofit.baseUrl(baseUrl)  }

        return retrofit.build()
    }

}