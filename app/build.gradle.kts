plugins {
    id("com.android.application")
    id("kotlin-android")
    id("com.google.gms.google-services")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(Versions.COMPILE_SDK)
    buildToolsVersion(Versions.BUILD_TOOLS)

    defaultConfig {
        applicationId = "com.airmee.devinterview"
        minSdkVersion(Versions.MIN_SDK)
        targetSdkVersion(Versions.TARGET_SDK)
        versionCode = Versions.VERSION_CODE
        versionName = Versions.VERSION_NAME
        multiDexEnabled = true

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            isDebuggable = true
//            applicationIdSuffix = ".debug"
            versionNameSuffix = "-dev"
        }
        getByName("release") {
            isMinifyEnabled = true
            isDebuggable = false
            isShrinkResources = true
            isZipAlignEnabled = true
            isJniDebuggable = false
            isRenderscriptDebuggable = false
//            signingConfig = signingConfigs.getByName("release")
        }
    }

    flavorDimensions("airmee")
    productFlavors{
        create("directAPK") {
            setDimension("airmee")
            setMatchingFallbacks("directAPK")
        }
        create("playStore") {
            setDimension("airmee")
            setMatchingFallbacks("playStore")
        }
    }
    compileOptions {
        // Flag to enable support for the new language APIs
//        isCoreLibraryDesugaringEnabled = true
        // Sets Java compatibility to Java 8
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures{
        dataBinding = true
    }
}

dependencies {
    implementation(project(":BusinessCore"))
//    implementation("com.android.tools:desugar_jdk_libs:1.1.5")
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Versions.KOTLIN}")
    implementation("androidx.core:core-ktx:${Versions.CORE_KTX}")
    implementation("androidx.appcompat:appcompat:${Versions.APP_COMPAT}")
    implementation("com.google.android.material:material:${Versions.MATERIAL}")
    implementation("com.android.support:design:${Versions.SUPPORT}")
    implementation("androidx.constraintlayout:constraintlayout:${Versions.CONSTRANIT_LAYOUT}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE}")
    implementation("androidx.lifecycle:lifecycle-extensions:${Versions.LIFECYCLE}")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:${Versions.LIFECYCLE}")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("org.koin:koin-androidx-scope:${Versions.KOIN_ANDROIDX}")
    implementation("org.koin:koin-androidx-viewmodel:${Versions.KOIN_ANDROIDX}")
    implementation("org.koin:koin-androidx-ext:${Versions.KOIN_ANDROIDX}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE}")
    implementation("androidx.cardview:cardview:${Versions.CARD_VIEW}")
    implementation("com.google.android.gms:play-services-location:18.0.0")

    testImplementation("junit:junit:${Versions.JUNIT}")
    androidTestImplementation("androidx.test.ext:junit:${Versions.EXT_JUNIT}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${Versions.ESPRESSO}")

    implementation("com.google.firebase:firebase-bom:28.0.1")
    implementation("com.google.firebase:firebase-analytics-ktx:19.0.0")
}