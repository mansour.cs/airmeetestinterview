package com.airmee.devinterview

import android.app.Application
import com.airmee.businesscore.BusinessCoreApplication
import com.airmee.businesscore.coreModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.Module

class AirmeeApp: BusinessCoreApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.INFO)
            androidContext(this@AirmeeApp)
            val moduleList = ArrayList<Module>()
            moduleList.addAll(coreModules)
            moduleList.add(viewModelsModule)
            modules(moduleList)
        }
    }
}