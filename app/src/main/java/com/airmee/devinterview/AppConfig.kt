package com.airmee.devinterview

import java.time.LocalDate

object AppConfig {
    var gpsEnabled = false
    var dateFrom: LocalDate? = null
    var dateTo: LocalDate? = null
}