package com.airmee.devinterview

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.airmee.devinterview.databinding.ActivityMainBinding
import com.airmee.devinterview.views.booked.BookedListFragment
import com.airmee.devinterview.views.search.ApartmentListFragment
import com.airmee.devinterview.views.user.UserFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {
    private val REQUEST_LOCATION = 1
    lateinit var dataBinding: ActivityMainBinding

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(dataBinding.root)

        dataBinding.navigationView.setOnNavigationItemSelectedListener(this)
        if (savedInstanceState == null) {
            openFragment(ApartmentListFragment.newInstance())
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    fun onLocationFound(onFound: (latitude: Double, longitude: Double) -> Unit) {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_LOCATION
            )
            return
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    val lat: Double = location.getLatitude()
                    val longi: Double = location.getLongitude()
                    onFound(lat, longi)
                } else {
                    Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show()
                }
            }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when(item.itemId) {
            R.id.navigation_search -> {
                openFragment(ApartmentListFragment.newInstance())
                return true
            }
            R.id.navigation_booked -> {
                openFragment(BookedListFragment.newInstance())
                return true
            }
            R.id.navigation_user -> {
                openFragment(UserFragment.newInstance())
                return true
            }
        }
        return false
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.commitNow()
    }
}