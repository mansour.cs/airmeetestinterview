package com.airmee.devinterview

import com.airmee.devinterview.views.booked.BookedListViewModel
import com.airmee.devinterview.views.booking.BookViewModel
import com.airmee.devinterview.views.search.ApartmentListViewModel
import com.airmee.devinterview.views.search.intents.SearchViewAction
import com.airmee.devinterview.views.search.intents.SearchViewEvent
import com.airmee.devinterview.views.user.UserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelsModule = module {
    viewModel { ApartmentListViewModel() }
    viewModel { BookedListViewModel() }
    viewModel { BookViewModel() }
    viewModel { UserViewModel() }
}