package com.airmee.devinterview.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel<VE: BaseViewEvent, VA: BaseViewAction>: ViewModel() {
    var actionLiveData = MutableLiveData<VA>()
    abstract fun onViewEvent(event: VE)
}