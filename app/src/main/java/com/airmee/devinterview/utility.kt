package com.airmee.devinterview

import android.view.View

fun visibleInvisible(visible: Boolean): Int {
    return if (visible) View.VISIBLE else View.INVISIBLE
}

fun visibleGone(visible: Boolean): Int {
    return if (visible) View.VISIBLE else View.GONE
}