package com.airmee.devinterview.views.booked

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.formatted
import com.airmee.devinterview.databinding.LayoutApartmentItemBinding
import com.airmee.devinterview.databinding.LayoutBookedApartmentItemBinding

class BookedListAdapter(
    var items: List<BookedApartmentEntity>,
    val onRowClicked: (position: Int, item: BookedApartmentEntity) -> Unit
    ): RecyclerView.Adapter<BookedListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = LayoutBookedApartmentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textViewName.text = items[position].apartmentItemEntity?.name
        holder.binding.textViewBedrooms.text = "${items[position].apartmentItemEntity?.bedrooms}"
        holder.binding.textViewCoordinates.text = "(${items[position].apartmentItemEntity?.latitude} , ${items[position].apartmentItemEntity?.longitude})"
        holder.binding.textViewDate.text = "${items[position].dateFrom.formatted()}  to  ${items[position].dateTo.formatted()}"

        holder.binding.root.setOnClickListener {
            onRowClicked?.invoke(position, items[position])
        }

    }

    override fun getItemCount(): Int = items.size



    inner class ViewHolder(val binding: LayoutBookedApartmentItemBinding): RecyclerView.ViewHolder(binding.root) {

    }
}