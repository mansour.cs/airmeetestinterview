package com.airmee.devinterview.views.booked

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.devinterview.R
import com.airmee.devinterview.databinding.BookedListFragmentBinding
import com.airmee.devinterview.views.booked.intents.BookedListViewAction
import com.airmee.devinterview.views.booked.intents.BookedListViewEvent
import com.airmee.devinterview.views.booking.BookFragment
import com.airmee.devinterview.views.search.ApartmentListAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class BookedListFragment : Fragment() {

    companion object {
        fun newInstance() = BookedListFragment()
    }

    private lateinit var adapter: BookedListAdapter
    private val viewModel by viewModel<BookedListViewModel>()
    private lateinit var dataBinding : BookedListFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = BookedListFragmentBinding.inflate(inflater, container, false)
        dataBinding.handlerFragment = this


        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        activity?.title = "Booked apartments"

        viewModel.actionLiveData.observe(viewLifecycleOwner) {
            when(it) {
                is BookedListViewAction.BookedListApartments -> refreshListData(it.items)
            }
        }

        viewModel.onViewEvent(BookedListViewEvent.ViewLoaded)
    }

    private fun initAdapter() {
        var linearLayoutManager = LinearLayoutManager(context)
        adapter = BookedListAdapter(listOf(), this::onRowClicked)
        dataBinding.recyclerBookedList.adapter = adapter
        dataBinding.recyclerBookedList.layoutManager = linearLayoutManager
    }

    private fun refreshListData(items: List<BookedApartmentEntity>) {
        adapter.items = items
        adapter.notifyDataSetChanged()
    }

    private fun onRowClicked(position: Int, item: BookedApartmentEntity) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.container, BookFragment.newInstance(item))
        transaction?.addToBackStack("BookFragment")
        transaction?.commit()
    }

}