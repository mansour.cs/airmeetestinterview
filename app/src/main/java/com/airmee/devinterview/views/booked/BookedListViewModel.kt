package com.airmee.devinterview.views.booked

import androidx.lifecycle.viewModelScope
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.features.book.service.IBookApartmentService
import com.airmee.devinterview.base.BaseViewModel
import com.airmee.devinterview.views.booked.intents.BookedListViewAction
import com.airmee.devinterview.views.booked.intents.BookedListViewEvent
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent

class BookedListViewModel : BaseViewModel<BookedListViewEvent, BookedListViewAction>() {
    private val bookService: IBookApartmentService by KoinJavaComponent.inject(IBookApartmentService::class.java)

    override fun onViewEvent(event: BookedListViewEvent) {
        when(event) {
            BookedListViewEvent.ViewLoaded -> loadBookedList()

        }
    }

    private fun loadBookedList() {
        viewModelScope.launch {
            val bookedList = bookService.getBookedApartmentList()
            if (bookedList.status == ServiceDataStatus.SUCCESS) {
                actionLiveData.postValue(BookedListViewAction.BookedListApartments(bookedList.data!!))
            } else {

            }
        }
    }

}