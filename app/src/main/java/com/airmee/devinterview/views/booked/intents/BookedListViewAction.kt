package com.airmee.devinterview.views.booked.intents

import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import com.airmee.devinterview.base.BaseViewAction

sealed class BookedListViewAction: BaseViewAction {
    data class BookedListApartments(val items: List<BookedApartmentEntity>): BookedListViewAction()
}
