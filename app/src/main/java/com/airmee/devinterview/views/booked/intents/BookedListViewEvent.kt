package com.airmee.devinterview.views.booked.intents

import com.airmee.devinterview.base.BaseViewEvent

sealed class BookedListViewEvent: BaseViewEvent {
    object ViewLoaded: BookedListViewEvent()
}
