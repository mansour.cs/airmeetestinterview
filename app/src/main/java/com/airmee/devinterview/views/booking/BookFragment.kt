package com.airmee.devinterview.views.booking

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airmee.businesscore.features.book.service.entities.BookedApartmentEntity
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.formatted
import com.airmee.devinterview.R
import com.airmee.devinterview.databinding.BookFragmentBinding
import com.airmee.devinterview.views.booking.intents.BookViewAction
import com.airmee.devinterview.views.booking.intents.BookViewEvent
import com.airmee.devinterview.views.search.intents.SearchViewEvent
import com.airmee.devinterview.visibleGone
import com.airmee.devinterview.visibleInvisible
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.LocalDate

class BookFragment(val apartmentItemEntity: ApartmentItemEntity) : Fragment() {

    companion object {
        fun newInstance(apartmentItemEntity: ApartmentItemEntity): BookFragment {
            val fragment = BookFragment(apartmentItemEntity)
            fragment.booked = false
            return fragment
        }

        fun newInstance(bookedApartmentEntity: BookedApartmentEntity) = BookFragment(bookedApartmentEntity)

    }

    constructor(bookedApartmentEntity: BookedApartmentEntity) : this(bookedApartmentEntity.apartmentItemEntity!!) {
        this.bookedApartmentEntity = bookedApartmentEntity
        booked = true
    }

    private var booked: Boolean = false
    private var bookedApartmentEntity: BookedApartmentEntity? = null
    private val viewModel by viewModel<BookViewModel>()
    private lateinit var dataBinding : BookFragmentBinding
    private var dateFrom = LocalDate.now()
    private var dateTo = dateFrom.plusDays(3)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = BookFragmentBinding.inflate(inflater, container, false)
        dataBinding.handlerFragment = this
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.textViewName.text = apartmentItemEntity.name
        dataBinding.textViewBedrooms.text = "${apartmentItemEntity.bedrooms}"
        dataBinding.textViewCoordinates.text = "(${apartmentItemEntity.latitude} , ${apartmentItemEntity.longitude})"
        if (booked) {
            dataBinding.textViewDate.text =
                "${bookedApartmentEntity?.dateFrom?.formatted()}  to  ${bookedApartmentEntity?.dateTo?.formatted()}"
            dataBinding.labelDate.text = "${getString(R.string.booked_date)}:"
            activity?.title = getString(R.string.cancel_book)
        } else {
            dataBinding.textViewDate.text =
                "${apartmentItemEntity.availableDateFrom.formatted()}  to  ${apartmentItemEntity.availableDateTo.formatted()}"
            dataBinding.labelDate.text = "${getString(R.string.available_date)}:"
            activity?.title = getString(R.string.book_apartment)
        }
        dataBinding.textViewFrom.visibility = visibleGone(!booked)
        dataBinding.imageCalendarFrom.visibility = visibleGone(!booked)
        dataBinding.textViewTo.visibility = visibleGone(!booked)
        dataBinding.imageCalendarTo.visibility = visibleGone(!booked)
        dataBinding.labelFrom.visibility = visibleInvisible(!booked)
        dataBinding.labelTo.visibility = visibleGone(!booked)

        dataBinding.textViewFrom.text = dateFrom.formatted()
        dataBinding.textViewTo.text = dateTo.formatted()
        dataBinding.buttonName = if (!booked) "Book" else "Delete"


        viewModel.actionLiveData.observe(viewLifecycleOwner) {
            when(it) {
                is BookViewAction.BookResult -> bookResultCheck(it)
                is BookViewAction.DeleteBookedResult -> deleteBookResultCheck(it)
            }
        }


    }

    fun bookApartment(view: View) {
        if (booked) {
            viewModel.onViewEvent(
                BookViewEvent.DeleteBookedApartment(apartmentItemEntity.id)
            )
        } else {
            viewModel.onViewEvent(
                BookViewEvent.BookNewApartment(
                    apartmentItemEntity.id,
                    dateFrom,
                    dateTo
                )
            )
        }
    }

    fun onDateFromClicked(view: View) {
        var datePicker = DatePickerDialog(requireContext(), { datePicker, y, m, d ->
            val selectedDate = LocalDate.of(y, m+1, d)
            if (selectedDate <= dateTo && selectedDate >= apartmentItemEntity.availableDateFrom) {
                dateFrom = selectedDate
                dataBinding.textViewFrom.text = dateFrom.formatted()
            }
        }, dateFrom.year, dateFrom.monthValue-1, dateFrom.dayOfMonth)
        datePicker.show()
    }

    fun onDateToClicked(view: View) {
        var datePicker = DatePickerDialog(requireContext(), { datePicker, y, m, d ->
            val selectedDate = LocalDate.of(y, m+1, d)
            if (dateFrom <= selectedDate && selectedDate <= apartmentItemEntity.availableDateTo) {
                dateTo = selectedDate
                dataBinding.textViewTo.text = dateTo.formatted()
            }
        }, dateTo.year, dateTo.monthValue-1, dateTo.dayOfMonth)

        datePicker.show()
    }

    private fun bookResultCheck(result: BookViewAction.BookResult) {
        if (result.success) {
            activity?.onBackPressed()
        }
    }

    private fun deleteBookResultCheck(result: BookViewAction.DeleteBookedResult) {
        if (result.success) {
            activity?.onBackPressed()
        }
    }

}