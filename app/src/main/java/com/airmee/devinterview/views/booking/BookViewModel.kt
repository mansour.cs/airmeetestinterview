package com.airmee.devinterview.views.booking

import androidx.lifecycle.viewModelScope
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.features.book.service.IBookApartmentService
import com.airmee.devinterview.base.BaseViewModel
import com.airmee.devinterview.views.booking.intents.BookViewAction
import com.airmee.devinterview.views.booking.intents.BookViewEvent
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject

class BookViewModel : BaseViewModel<BookViewEvent, BookViewAction>() {
    private val bookService: IBookApartmentService by inject(IBookApartmentService::class.java)

    override fun onViewEvent(event: BookViewEvent) {
        when(event) {
            is BookViewEvent.BookNewApartment -> bookApartment(event)
            is BookViewEvent.DeleteBookedApartment -> deleteBookedApartment(event)
        }
    }

    private fun deleteBookedApartment(event: BookViewEvent.DeleteBookedApartment) {
        viewModelScope.launch {
            val deleteResult = bookService.deleteBookedApartment(event.id)
            if (deleteResult.status == ServiceDataStatus.SUCCESS) {
                actionLiveData.postValue(BookViewAction.DeleteBookedResult(true))
            } else {
                actionLiveData.postValue(
                    BookViewAction.DeleteBookedResult(
                        false,
                        deleteResult.serviceMessage
                    )
                )
            }
        }


    }

    private fun bookApartment(event: BookViewEvent.BookNewApartment) {
        viewModelScope.launch {
            val newBookResult = bookService.bookNewApartment(event.id, event.dateFrom, event.dateTo)
            if (newBookResult.status == ServiceDataStatus.SUCCESS) {
                actionLiveData.postValue(BookViewAction.BookResult(true))
            } else {
                actionLiveData.postValue(BookViewAction.BookResult(false, newBookResult.serviceMessage))
            }
        }
    }

}