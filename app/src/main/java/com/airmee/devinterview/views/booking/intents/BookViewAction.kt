package com.airmee.devinterview.views.booking.intents

import com.airmee.businesscore.baseClasses.service.ServiceMessage
import com.airmee.devinterview.base.BaseViewAction
import com.airmee.devinterview.base.BaseViewEvent

sealed class BookViewAction: BaseViewAction {
    data class BookResult(val success: Boolean, val serviceMessage: ServiceMessage? = null): BookViewAction()
    data class DeleteBookedResult(val success: Boolean, val serviceMessage: ServiceMessage? = null): BookViewAction()
}