package com.airmee.devinterview.views.booking.intents

import com.airmee.devinterview.base.BaseViewEvent
import java.time.LocalDate

sealed class BookViewEvent: BaseViewEvent {
    data class BookNewApartment(val id: String, val dateFrom: LocalDate, val dateTo: LocalDate): BookViewEvent()
    data class DeleteBookedApartment(val id: String): BookViewEvent()
}
