package com.airmee.devinterview.views.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.formatted
import com.airmee.devinterview.databinding.LayoutApartmentItemBinding

class ApartmentListAdapter(
    var items: List<ApartmentItemEntity>,
    val onRowClicked: (position: Int, item: ApartmentItemEntity) -> Unit
    ): RecyclerView.Adapter<ApartmentListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflatedView = LayoutApartmentItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textViewName.text = items[position].name
        holder.binding.textViewBedrooms.text = "${items[position].bedrooms}"
        holder.binding.textViewCoordinates.text = "(${items[position].latitude} , ${items[position].longitude})"
        holder.binding.textViewDate.text = "${items[position].availableDateFrom.formatted()}  to  ${items[position].availableDateTo.formatted()}"

        holder.binding.root.setOnClickListener {
            onRowClicked?.invoke(position, items[position])
        }

    }

    override fun getItemCount(): Int = items.size



    inner class ViewHolder(val binding: LayoutApartmentItemBinding): RecyclerView.ViewHolder(binding.root) {

    }
}