package com.airmee.devinterview.views.search

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.businesscore.formatted
import com.airmee.devinterview.AppConfig
import com.airmee.devinterview.MainActivity
import com.airmee.devinterview.R
import com.airmee.devinterview.databinding.FragmentApartmentListBinding
import com.airmee.devinterview.views.booking.BookFragment
import com.airmee.devinterview.views.search.intents.SearchViewAction
import com.airmee.devinterview.views.search.intents.SearchViewEvent
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.time.LocalDate

class ApartmentListFragment : Fragment() {
    private val viewModel by viewModel<ApartmentListViewModel>()
    lateinit var dataBinding: FragmentApartmentListBinding
    lateinit var adapter: ApartmentListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var bedrooms: Int? = null
    private var dateFrom = LocalDate.now()
    private var dateTo = dateFrom.plusDays(3)
    private var latitude: Double? = null
    private var longitude: Double? = null

    companion object {
        fun newInstance() = ApartmentListFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dataBinding = FragmentApartmentListBinding.inflate(inflater, container, false)
        dataBinding.handlerFragment = this

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initBedroomsSpinner()

        activity?.title = "Search apartments"

        viewModel.actionLiveData.observe(viewLifecycleOwner) {
            when(it) {
                is SearchViewAction.InitialValues -> initializeViewValues(it)
                is SearchViewAction.ApartmentList -> refreshListData(it.apartments)
            }
        }
        viewModel.onViewEvent(SearchViewEvent.ViewLoaded)
    }

    private fun initializeViewValues(initialValues: SearchViewAction.InitialValues) {
        initialValues.dateFrom?.let { dateFrom = it }
        initialValues.dateTo?.let { dateTo = it }
        dataBinding.textViewFrom.text = dateFrom.formatted()
        dataBinding.textViewTo.text = dateTo.formatted()
        if (initialValues.gpsEnabled) {
            (activity as? MainActivity)?.onLocationFound { latitude, longitude ->
                this.latitude = latitude
                this.longitude = longitude
                viewModel.onViewEvent(
                    SearchViewEvent.Search(
                        bedrooms,
                        dateFrom,
                        dateTo,
                        latitude,
                        longitude
                    )
                )
            }
        } else {
            this.latitude = null
            this.longitude = null
            viewModel.onViewEvent(
                SearchViewEvent.Search(
                    bedrooms,
                    dateFrom,
                    dateTo,
                    latitude,
                    longitude
                )
            )
        }
    }

    private fun initAdapter() {
        linearLayoutManager = LinearLayoutManager(context)
        adapter = ApartmentListAdapter(listOf(), this::onRowClicked)
        dataBinding.recyclerApartmentList.adapter = adapter
        dataBinding.recyclerApartmentList.layoutManager = linearLayoutManager
    }

    private fun refreshListData(items: List<ApartmentItemEntity>) {
        adapter.items = items
        adapter.notifyDataSetChanged()
    }

    private fun onRowClicked(position: Int, item: ApartmentItemEntity) {
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.replace(R.id.container, BookFragment.newInstance(item))
        transaction?.addToBackStack("BookFragment")
        transaction?.commit()
    }

    private fun initBedroomsSpinner() {
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.bedrooms_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            dataBinding.spinnerNumBedrooms.adapter = adapter
            dataBinding.spinnerNumBedrooms.onItemSelectedListener = object :OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    bedrooms = if (p2-1 >= 0) p2-1 else null
                    viewModel.onViewEvent(SearchViewEvent.Search(bedrooms, dateFrom, dateTo, latitude, longitude))
                }

                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

            }
        }
    }

    fun onDateFromClicked(view: View) {
        var datePicker = DatePickerDialog(requireContext(), { datePicker, y, m, d ->
            val selectedDate = LocalDate.of(y, m+1, d)
            if (selectedDate <= dateTo) {
                dateFrom = selectedDate
                dataBinding.textViewFrom.text = dateFrom.formatted()
                viewModel.onViewEvent(SearchViewEvent.Search(bedrooms, dateFrom, dateTo, latitude, longitude))
            }
        }, dateFrom.year, dateFrom.monthValue-1, dateFrom.dayOfMonth)
        datePicker.show()
    }

    fun onDateToClicked(view: View) {
        var datePicker = DatePickerDialog(requireContext(), { datePicker, y, m, d ->
            val selectedDate = LocalDate.of(y, m+1, d)
            if (dateFrom <= selectedDate) {
                dateTo = selectedDate
                dataBinding.textViewTo.text = dateTo.formatted()
                viewModel.onViewEvent(SearchViewEvent.Search(bedrooms, dateFrom, dateTo, latitude, longitude))
            }
        }, dateTo.year, dateTo.monthValue-1, dateTo.dayOfMonth)

        datePicker.show()
    }
}