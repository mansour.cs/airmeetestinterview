package com.airmee.devinterview.views.search

import androidx.lifecycle.viewModelScope
import com.airmee.businesscore.baseClasses.service.ServiceDataStatus
import com.airmee.businesscore.features.search.service.IApartmentSearchService
import com.airmee.businesscore.features.user.service.IUserService
import com.airmee.devinterview.AppConfig
import com.airmee.devinterview.base.BaseViewModel
import com.airmee.devinterview.views.search.intents.SearchViewAction
import com.airmee.devinterview.views.search.intents.SearchViewEvent
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject
import java.time.LocalDate

class ApartmentListViewModel: BaseViewModel<SearchViewEvent, SearchViewAction>() {
    private val searchService: IApartmentSearchService by inject(IApartmentSearchService::class.java)
    private val userService: IUserService by inject(IUserService::class.java)

    override fun onViewEvent(event: SearchViewEvent) {
        when(event) {
            is SearchViewEvent.ViewLoaded -> {initializeValues()}
            is SearchViewEvent.Search -> search(event.bedrooms, event.dateFrom, event.dateTo, event.latitude, event.longitude)

        }
    }

    private fun initializeValues() {
        actionLiveData.postValue(SearchViewAction.InitialValues(AppConfig.dateFrom, AppConfig.dateTo, AppConfig.gpsEnabled))
    }

    private fun search(bedrooms: Int? = null, dateFrom: LocalDate, dateTo: LocalDate,
                       latitude:Double? = null,
                       longitude: Double? = null) {
        AppConfig.dateFrom = dateFrom
        AppConfig.dateTo = dateTo

        viewModelScope.launch {
            //Determine using default coordinates or live gps coordinate
            var user = if (latitude == null || longitude == null) {
                userService.getDefaultUser()
            } else {
                userService.getDefaultUser(latitude, longitude)
            }

            val searchData = searchService.getApartmentList(user, bedrooms, dateFrom, dateTo)
            if (searchData.status == ServiceDataStatus.SUCCESS) {
                searchData.data?.let {
                    actionLiveData.postValue(SearchViewAction.ApartmentList(it))
                }
            } else {

            }
        }



    }


}