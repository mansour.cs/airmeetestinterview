package com.airmee.devinterview.views.search.intents

import com.airmee.businesscore.features.search.service.entities.ApartmentItemEntity
import com.airmee.devinterview.base.BaseViewAction
import java.time.LocalDate

sealed class SearchViewAction: BaseViewAction {
    data class InitialValues(val dateFrom: LocalDate?, val dateTo: LocalDate?, val gpsEnabled: Boolean): SearchViewAction()
    data class ApartmentList(val apartments: List<ApartmentItemEntity>): SearchViewAction()
}
