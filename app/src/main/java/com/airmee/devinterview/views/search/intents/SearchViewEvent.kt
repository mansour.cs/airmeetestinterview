package com.airmee.devinterview.views.search.intents

import com.airmee.devinterview.base.BaseViewEvent
import java.time.LocalDate

sealed class SearchViewEvent: BaseViewEvent {
    object ViewLoaded: SearchViewEvent()
    data class Search(val bedrooms: Int?,
                      val dateFrom: LocalDate,
                      val dateTo: LocalDate,
                      val latitude:Double? = null,
                      val longitude: Double? = null): SearchViewEvent()
}