package com.airmee.devinterview.views.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.Switch
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import com.airmee.businesscore.features.user.service.entities.UserEntity
import com.airmee.devinterview.AppConfig
import com.airmee.devinterview.MainActivity
import com.airmee.devinterview.databinding.UserFragmentBinding
import com.airmee.devinterview.views.search.intents.SearchViewAction
import com.airmee.devinterview.views.search.intents.SearchViewEvent
import com.airmee.devinterview.views.user.intents.UserViewAction
import com.airmee.devinterview.views.user.intents.UserViewEvent
import com.airmee.devinterview.visibleInvisible
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserFragment : Fragment() {

    companion object {
        fun newInstance() = UserFragment()
    }

    private val viewModel by viewModel<UserViewModel>()
    private lateinit var dataBinding: UserFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = UserFragmentBinding.inflate(inflater, container, false)
        dataBinding.handlerFragment = this
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.title = "User properties"
        dataBinding.switchEnableGPS.isChecked = AppConfig.gpsEnabled
        onCheckSwitch(AppConfig.gpsEnabled)
        viewModel.actionLiveData.observe(viewLifecycleOwner) {
            when(it) {
                is UserViewAction.UserProperties -> fillData(it.user)
            }
        }
        viewModel.onViewEvent(UserViewEvent.ViewLoaded)
    }

    fun fillData(user: UserEntity) {
        dataBinding.textViewName.text = user.name
        dataBinding.textViewFamily.text = user.family
        dataBinding.textViewDefaultLoc.text = "(${user.defLatitude} , ${user.defLongitude})"
        user.latitude.let { dataBinding.textViewGPSLoc.text = "(${user.latitude} , ${user.longitude})" }
    }

    fun onCheckedChanged(switch: CompoundButton?, checked: Boolean) {
        onCheckSwitch(checked)

    }

    private fun onCheckSwitch(checked: Boolean) {
        dataBinding.labelGPSLoc.visibility = visibleInvisible(checked)
        dataBinding.textViewGPSLoc.visibility = visibleInvisible(checked)
        AppConfig.gpsEnabled = checked
        (activity as? MainActivity)?.onLocationFound { latitude, longitude ->
            viewModel.onViewEvent(UserViewEvent.GpsStatusChanged(checked, latitude, longitude))
        }
    }

}