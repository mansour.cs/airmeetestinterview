package com.airmee.devinterview.views.user

import com.airmee.businesscore.features.user.service.IUserService
import com.airmee.devinterview.AppConfig
import com.airmee.devinterview.base.BaseViewModel
import com.airmee.devinterview.views.user.intents.UserViewAction
import com.airmee.devinterview.views.user.intents.UserViewEvent
import org.koin.java.KoinJavaComponent.inject

class UserViewModel : BaseViewModel<UserViewEvent, UserViewAction>() {
    private val service: IUserService by inject(IUserService::class.java)

    override fun onViewEvent(event: UserViewEvent) {
        when(event) {
            is UserViewEvent.GpsStatusChanged -> switchGpsStatus(event)
            is UserViewEvent.ViewLoaded -> deliverDefaultUser()
        }
    }

    private fun switchGpsStatus(event: UserViewEvent.GpsStatusChanged) {
        if (event.enabled) {
            actionLiveData.postValue(
                UserViewAction.UserProperties(
                    service.getDefaultUser(
                        event.latitude!!,
                        event.longitude!!
                    )
                )
            )
        } else {
            deliverDefaultUser()
        }
    }

    private fun deliverDefaultUser() {
        actionLiveData.postValue(UserViewAction.UserProperties(service.getDefaultUser()))
    }

}