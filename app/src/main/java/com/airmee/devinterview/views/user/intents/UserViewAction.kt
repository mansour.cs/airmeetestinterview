package com.airmee.devinterview.views.user.intents

import com.airmee.businesscore.features.user.service.entities.UserEntity
import com.airmee.devinterview.base.BaseViewAction

sealed class UserViewAction: BaseViewAction {
    data class UserProperties(val user: UserEntity): UserViewAction()
}
