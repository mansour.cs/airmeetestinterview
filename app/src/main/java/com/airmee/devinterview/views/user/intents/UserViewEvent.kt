package com.airmee.devinterview.views.user.intents

import com.airmee.devinterview.base.BaseViewEvent
import com.airmee.devinterview.views.search.intents.SearchViewEvent

sealed class UserViewEvent: BaseViewEvent {
    object ViewLoaded: UserViewEvent()
    data class GpsStatusChanged(val enabled: Boolean, val latitude: Double?, val longitude: Double?): UserViewEvent()
}
