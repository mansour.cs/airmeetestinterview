// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
        mavenCentral()
        mavenLocal()
        maven {  url = uri("https://jitpack.io")}
        maven {  url = uri("https://maven.google.com")  }
    }
    dependencies {
        classpath("com.android.tools.build:gradle:4.2.1")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.KOTLIN}")
        classpath("org.koin:koin-gradle-plugin:${Versions.KOIN}")
        classpath("com.google.gms:google-services:4.3.8")
        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle.kts.kts files
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        mavenCentral()
        mavenLocal()
        maven {  url = uri("https://jitpack.io")  }
        maven {  url = uri("https://maven.google.com")  }
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
