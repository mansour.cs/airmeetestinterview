
object Versions {
    //app
    const val VERSION_CODE = 1
    const val VERSION_NAME = "1.0"


    //sdk
    const val KOTLIN = "1.4.32"
    const val COMPILE_SDK = 29
    const val TARGET_SDK = 29
    const val MIN_SDK = 26
    const val BUILD_TOOLS = "30.0.3"
    const val CORE_KTX = "1.3.2"
    const val APP_COMPAT = "1.2.0"
    const val MATERIAL = "1.3.0"
    const val SUPPORT = "27.0.2"
    const val CONSTRANIT_LAYOUT = "2.0.4"
    const val JUNIT = "4.13.2"
    const val EXT_JUNIT = "1.1.2"
    const val ESPRESSO = "3.3.0"
    const val ANNOTATION = "1.1.0"
    const val LIFECYCLE = "2.2.0"
    const val ROOM = "2.3.0"


    //3th party versions
    const val OKHTTP3 = "4.9.0"
    const val RETROFIT = "2.9.0"
    const val RXJAVA2 = "2.2.19"
    const val KOIN = "2.2.2"
    const val KOIN_ANDROIDX = "2.2.2"
    const val CARD_VIEW = "1.0.0"
}